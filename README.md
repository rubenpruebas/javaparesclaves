Primero debes generar un par de claves
<br>
Si le asignas de nombre 'asd' se guardarán como 'asd.pub' y 'asd.pri'
<br>
<br>
Luego selecciona la opción de crear un mensaje cifrado
<br>
Debes proporcionar el mensaje, la clave privada y el nombre del archivo
<br>
El archivo firmado se guardará como nombreArchivo.firma
<br>
<br>
Para descifrar y verificar la firma proporciona la clave pública y el archivo
<br>
<br>

![image info](gitlabclaves.png)