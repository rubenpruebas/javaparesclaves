package ejercicio;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

public class CifradorFirmador {
    private final String algoritmo;
    private final PrivateKey privateKey;
    private final String mensajeNormal;
    private final Signature firma;

    public CifradorFirmador(String al, String rutaPri, String msg) throws Exception {
        algoritmo = al;
        privateKey = Tools.getPrivateKeyByFileName(rutaPri);
        firma = Signature.getInstance("SHA256with" + algoritmo);
        firma.initSign(privateKey);
        mensajeNormal = msg;
    }

    public void cifrarToFile(String path) {
        try {
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            byte[] mensajeCifrado = cipher.doFinal(mensajeNormal.getBytes());
            Tools.writeBytesToFile(mensajeCifrado, path);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException |
                 InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public void firmarToFileFromFile(String pathMensajeCifrado, String pathMensajeFirmado) {
        try {
            firma.update(Tools.readBytesFromFile(pathMensajeCifrado));
            Tools.writeBytesToFile(firma.sign(), pathMensajeFirmado);
        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
    }
}
