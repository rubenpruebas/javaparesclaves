package ejercicio;

import java.security.*;

public class KeysGenerator {
    private PublicKey publicKey;
    private PrivateKey privateKey;

    private String algoritmo; // RSA

    public KeysGenerator(String a) {
        algoritmo = a;
        generate();
    }

    private void generate() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(algoritmo);
            KeyPair par = keyPairGenerator.generateKeyPair();
            publicKey = par.getPublic();
            privateKey = par.getPrivate();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void exportToFile(String path) {
        Tools.writeBytesToFile(publicKey.getEncoded(), path + ".pub");
        Tools.writeBytesToFile(privateKey.getEncoded(), path + ".pri");
    }

}
