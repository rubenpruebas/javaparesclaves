package ejercicio;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

public class DescifadorVerificador {
    private String algoritmo;
    private PublicKey publicKey;
    private Signature firma;

    public DescifadorVerificador(String algo, String pathPublicKey) {
        try {
            algoritmo = algo;
            publicKey = Tools.getPublicKeyByFileName(pathPublicKey);
            firma = Signature.getInstance("SHA256with" + algoritmo);
            firma.initVerify(publicKey);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] descifrarMensajeFromFile(String pathCifrado) {
        try {
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            return cipher.doFinal(Tools.readBytesFromFile(pathCifrado));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException |
                 InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean verificarFirma(String pathCifrado, String pathMensajeFirmado) {
        try {
            firma.update(Tools.readBytesFromFile(pathCifrado));
            return firma.verify(Tools.readBytesFromFile(pathMensajeFirmado));
        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
    }
}
