package ejercicio;

import java.util.Scanner;

public class Main {
    private final static String ALGORITMO = "RSA";

    public static void main(String[] args) {
        try {
            iniciar();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void iniciar() throws Exception {
        int opcionElegida = 0;
        Scanner sca = new Scanner(System.in);
        do {
            System.out.println("\n\n1. Generar claves");
            System.out.println("2. Cifrar y firmar mensaje");
            System.out.println("3. Descifrar y validar firma");
            System.out.println("0. Salir");
            System.out.print("Opcion -> ");
            opcionElegida = Integer.parseInt(sca.nextLine());
            switch (opcionElegida) {
                case 1:
                    KeysGenerator gen = new KeysGenerator(ALGORITMO);
                    System.out.print("\nNombre de las claves -> ");
                    String nombreClaves = sca.nextLine();
                    gen.exportToFile(nombreClaves);
                    break;
                case 2:
                    System.out.print("\nMensaje -> ");
                    String mensajeACifrar = sca.nextLine();
                    System.out.print("\nFichero clave privada -> ");
                    String ficheroClavePrivada = sca.nextLine();
                    System.out.print("\nNombre del nuevo archivo cifrado -> ");
                    String nombreArchivoCifrado = sca.nextLine();
                    CifradorFirmador cifrador = new CifradorFirmador(ALGORITMO, ficheroClavePrivada, mensajeACifrar);
                    cifrador.cifrarToFile(nombreArchivoCifrado);
                    cifrador.firmarToFileFromFile(nombreArchivoCifrado, nombreArchivoCifrado + ".firma");
                    System.out.println("\n\nMensaje cifrado y firmado correctamente");
                    break;
                case 3:
                    System.out.println("\nFichero clave publica -> ");
                    String ficheroClavePublica = sca.nextLine();
                    DescifadorVerificador descifadorVerificador = new DescifadorVerificador(ALGORITMO, ficheroClavePublica);
                    System.out.print("\nNombre del archivo cifrado -> ");
                    String nombreArchivoCifrado2 = sca.nextLine();
                    System.out.println("\n\nMensaje descifrado: " + new String(descifadorVerificador.descifrarMensajeFromFile(nombreArchivoCifrado2)));

                    if (descifadorVerificador.verificarFirma(nombreArchivoCifrado2, nombreArchivoCifrado2 + ".firma"))
                        System.out.println("La firma se ha verificado correctamente");
                    else System.out.println("La firma no se ha verificado correctamente");
                    break;
                case 0:
                    System.out.println("Saliendo...");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        } while (opcionElegida != 0);
    }
}
